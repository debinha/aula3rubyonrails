class Commit < ApplicationRecord
    validates :text, presence: true
end
