class Friend < ApplicationRecord
  belongs_to :user, class_name: "User"

  validates :name, :nickname, :age, presence: true
end
