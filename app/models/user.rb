class User < ApplicationRecord
    has_many :post
    has_many :friend, thought: :friend
    validates :name, :nickname, :email, presence: true
    validates :nickname, :email, uniqueness: true
end
