class Like < ApplicationRecord
  belongs_to :user
  validates :smileface, :sadface, :heartface, presence: true
end
