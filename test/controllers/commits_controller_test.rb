require 'test_helper'

class CommitsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @commit = commits(:one)
  end

  test "should get index" do
    get commits_url, as: :json
    assert_response :success
  end

  test "should create commit" do
    assert_difference('Commit.count') do
      post commits_url, params: { commit: { text: @commit.text } }, as: :json
    end

    assert_response 201
  end

  test "should show commit" do
    get commit_url(@commit), as: :json
    assert_response :success
  end

  test "should update commit" do
    patch commit_url(@commit), params: { commit: { text: @commit.text } }, as: :json
    assert_response 200
  end

  test "should destroy commit" do
    assert_difference('Commit.count', -1) do
      delete commit_url(@commit), as: :json
    end

    assert_response 204
  end
end
