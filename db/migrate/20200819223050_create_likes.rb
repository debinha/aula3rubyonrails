class CreateLikes < ActiveRecord::Migration[5.2]
  def change
    create_table :likes do |t|
      t.references :user, foreign_key: true
      t.string :smileface
      t.string :heartface
      t.string :sadface

      t.timestamps
    end
  end
end
